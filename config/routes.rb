Rails.application.routes.draw do
  localized do
    devise_for :users
    get 'admin', to: 'admin#index'
    root to: 'home#index'
  end
end
